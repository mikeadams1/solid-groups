import Avatar from "@material-ui/core/Avatar";
import LoadingIcon from "@material-ui/icons/QueryBuilder";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import LinkIcon from "@material-ui/icons/Link";

const useStyles = makeStyles(() => ({
  square: {
    width: "100%",
    height: "100%",
  },
  icon: {
    color: "rgba(255, 255, 255, 0.54)",
  },
}));

export const Member = ({ ready, ...props }) => {
  const classes = useStyles();
  return ready ? (
    <Profile classes={classes} {...props} />
  ) : (
    <Placeholder classes={classes} />
  );
};

const Placeholder = ({ classes }) => (
  <Avatar variant="square" className={classes.square}>
    <LoadingIcon className={classes.square} />
  </Avatar>
);

const Profile = ({ classes, webId, name, imageSrc }) => (
  <Avatar variant="square" className={classes.square}>
    <Avatar variant="square" src={imageSrc} className={classes.square} />
    <GridListTileBar
      title={name}
      actionIcon={
        <IconButton
          aria-label={`WebID: ${webId}`}
          component="a"
          href={webId}
          className={classes.icon}
        >
          <LinkIcon />
        </IconButton>
      }
    />
  </Avatar>
);
