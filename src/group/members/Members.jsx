import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import { useMembers } from "./useMembers";
import { Member } from "./Member";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper,
    padding: "1rem",
  },
  gridList: {
    width: 500,
  },
}));

export function Members({ group }) {
  const classes = useStyles();
  const { members } = useMembers(group);

  return (
    <div className={classes.root}>
      <GridList cellHeight={180} className={classes.gridList}>
        {members.map((person) => (
          <GridListTile key={person.webId}>
            <Member {...person} />
          </GridListTile>
        ))}
      </GridList>
    </div>
  );
}
