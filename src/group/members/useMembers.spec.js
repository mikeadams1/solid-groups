import { withHook } from "../../test-utils";
import { useMembers } from "./useMembers";
import { useServiceContext } from "../../service";
import { act } from "react-dom/test-utils";

jest.mock("../../service");

describe("when using members", () => {
  let renderCycle;
  let render;

  let loadProfile;

  beforeEach(() => {
    loadProfile = jest.fn();
    useServiceContext.mockReturnValue({ loadProfile });
    ({ render, renderCycle } = withHook(useMembers));
  });

  describe("of an empty group", () => {
    beforeEach(() => {
      act(() => {
        render({ uri: "group uri", members: [] });
      });
    });

    it("the list is empty", () => {
      expect(renderCycle[0].members).toEqual([]);
    });
  });

  describe("of a group with three members", () => {
    beforeEach(async () => {
      loadProfile.mockResolvedValueOnce({
        webId: "https://alice.example#me",
        name: "Alice",
      });
      loadProfile.mockResolvedValueOnce({
        webId: "https://bob.example#me",
        name: "Bob",
      });
      loadProfile.mockResolvedValueOnce({
        webId: "https://claire.example#me",
        name: "Claire",
      });
      await act(async () => {
        await render({
          uri: "group uri",
          members: [
            {
              webId: "https://alice.example#me",
            },
            {
              webId: "https://bob.example#me",
            },
            {
              webId: "https://claire.example#me",
            },
          ],
        });
      });
    });

    it("the list has three members initially", () => {
      expect(renderCycle[0].members).toEqual([
        {
          webId: "https://alice.example#me",
        },
        {
          webId: "https://bob.example#me",
        },
        {
          webId: "https://claire.example#me",
        },
      ]);
    });

    it("starts loading all three member", () => {
      expect(renderCycle[1].members).toEqual([
        {
          webId: "https://alice.example#me",
          loading: true,
        },
        {
          webId: "https://bob.example#me",
          loading: true,
        },
        {
          webId: "https://claire.example#me",
          loading: true,
        },
      ]);
    });

    it("finishes loading alice", () => {
      expect(renderCycle[2].members).toEqual([
        {
          webId: "https://alice.example#me",
          loading: false,
          ready: true,
          name: "Alice",
        },
        {
          webId: "https://bob.example#me",
          loading: true,
        },
        {
          webId: "https://claire.example#me",
          loading: true,
        },
      ]);
    });

    it("finishes loading bob", () => {
      expect(renderCycle[3].members).toEqual([
        {
          webId: "https://alice.example#me",
          loading: false,
          ready: true,
          name: "Alice",
        },
        {
          webId: "https://bob.example#me",
          loading: false,
          ready: true,
          name: "Bob",
        },
        {
          webId: "https://claire.example#me",
          loading: true,
        },
      ]);
    });

    it("finishes loading claire", () => {
      expect(renderCycle[4].members).toEqual([
        {
          webId: "https://alice.example#me",
          loading: false,
          ready: true,
          name: "Alice",
        },
        {
          webId: "https://bob.example#me",
          loading: false,
          ready: true,
          name: "Bob",
        },
        {
          webId: "https://claire.example#me",
          loading: false,
          ready: true,
          name: "Claire",
        },
      ]);
    });
  });
});
