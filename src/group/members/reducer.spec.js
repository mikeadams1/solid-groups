import reducer from "./reducer";

describe("members reducer", () => {
  describe("when alice's profile starts loading", () => {
    let newState;
    beforeEach(() => {
      newState = reducer(
        [
          {
            webId: "https://alice.example#me",
          },
          {
            webId: "https://bob.example#me",
          },
          {
            webId: "https://claire.example#me",
          },
        ],
        {
          type: "PROFILE_LOADING",
          payload: { webId: "https://alice.example#me" },
        }
      );
    });

    it("should indicate that alices' profile is loading", () => {
      const alice = find(newState, "https://alice.example#me");
      expect(alice.loading).toBe(true);
    });

    it("should not change other entries", () => {
      const bob = find(newState, "https://bob.example#me");
      const claire = find(newState, "https://claire.example#me");
      expect(bob).toEqual({
        webId: "https://bob.example#me",
      });
      expect(claire).toEqual({
        webId: "https://claire.example#me",
      });
      expect(newState.length).toBe(3);
    });
  });

  describe("when alice's profile finished loading", () => {
    let newState;
    beforeEach(() => {
      newState = reducer(
        [
          {
            webId: "https://alice.example#me",
            loading: true,
          },
          {
            webId: "https://bob.example#me",
          },
          {
            webId: "https://claire.example#me",
          },
        ],
        {
          type: "PROFILE_LOADED",
          payload: { webId: "https://alice.example#me", name: "Alice" },
        }
      );
    });
    it("then alice has a name", () => {
      const alice = find(newState, "https://alice.example#me");
      expect(alice.name).toBe("Alice");
    });

    it("then alice does not indicate loading", () => {
      const alice = find(newState, "https://alice.example#me");
      expect(alice.loading).toBe(false);
    });

    it("then alice's profile is ready to show", () => {
      const alice = find(newState, "https://alice.example#me");
      expect(alice.ready).toBe(true);
    });
  });
});

function find(newState, webId) {
  return newState.find((it) => it.webId === webId);
}
