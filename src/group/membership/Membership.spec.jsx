import React from "react";
import {useMembership} from "./useMembership";
import {Membership} from "./Membership";
import {JoinGroup} from "../join/JoinGroup";
import {shallow} from "enzyme";
import {LogInToJoin} from "./LogInToJoin";
import {Greeting} from "./Greeting";

jest.mock("./useMembership");

describe("Membership", () => {
  describe("when user is not yet member of the group", () => {
    beforeEach(() => {
      useMembership.mockReturnValue({ loggedIn: true, userIsMember: false });
    });
    it("then she can join the group", () => {
      const result = shallow(<Membership />);
      expect(result.find(JoinGroup)).toExist();
    });
  });

  describe("when user is already member of the group", () => {
    beforeEach(() => {
      useMembership.mockReturnValue({ loggedIn: true, userIsMember: true });
    });
    it("then she can not request joining again", () => {
      const result = shallow(<Membership />);
      expect(result.find(JoinGroup)).not.toExist();
    });
    it("then the member is greeted", () => {
      const result = shallow(<Membership />);
      expect(result.find(Greeting)).toExist();
    });
  });

  describe("when user is not logged in", () => {
    beforeEach(() => {
      useMembership.mockReturnValue({ loggedIn: false });
    });

    it("then a log in hint is shown", () => {
      const result = shallow(<Membership />);
      expect(result).toContainReact(<LogInToJoin />);
    });
  });
});
