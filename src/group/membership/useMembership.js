import { useSession } from "../../authentication";

export const useMembership = (group) => {
  const { loggedIn, user } = useSession();
  const userIsMember = loggedIn && membersContainUser(group, user);
  return { loggedIn, userIsMember };
};

function membersContainUser(group, user) {
  return group.members.some((it) => it.webId === user.webId);
}
