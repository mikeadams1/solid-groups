import React from "react";
import {useSession} from "../../authentication";


export const Greeting = () =>  {
    const {user} = useSession();
    return <div>Hello, {user.name}. It's wonderful that you are part of our group. ❤</div>
}