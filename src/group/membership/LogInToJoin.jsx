import React from "react";
import { Login } from "../authentication/Login";

export const LogInToJoin = () => (
  <div>
    Please <Login variant="contained" color="primary" /> to join the group
  </div>
);
