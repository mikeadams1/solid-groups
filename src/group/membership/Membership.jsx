import React from "react";
import { JoinGroup } from "../join/JoinGroup";
import { useMembership } from "./useMembership";
import {LogInToJoin} from "./LogInToJoin";
import {Greeting} from "./Greeting";

export const Membership = ({ group }) => {
  const { loggedIn, userIsMember } = useMembership(group);
  if (!loggedIn) {
    return <LogInToJoin />
  }
  return userIsMember ? <Greeting /> : <JoinGroup group={group} />;
};
