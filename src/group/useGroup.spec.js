import {useAsync} from "react-use";
import {useGroup} from "./useGroup";
import {useServiceContext} from "../service";

jest.mock("react-use");
jest.mock("../service");

describe("useGroup", () => {
    beforeEach(() => {
        useServiceContext.mockReturnValue({});
    });

    it("should return loaded group data", () => {
        useAsync.mockReturnValue({
            value: {foo: "bar"},
        });
        const {group} = useGroup("https://group.example/group/main#we");
        expect(group.foo).toBe("bar");
    });

    it("should return fallback name if none loaded", () => {
        useAsync.mockReturnValue({
            value: {},
        });
        const {group} = useGroup("https://group.example/group/main#we");
        expect(group.name).toBe("Unnamed Group");
    });

    it("should return loading & error flag", () => {
        useAsync.mockReturnValue({
            loading: true,
            error: true,
        });
        const result = useGroup("https://group.example/group/main#we");
        expect(result.loading).toBe(true);
        expect(result.error).toBe(true);
    });
});
