import React, {useState} from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import {useSession} from "../../authentication";
import LogoutIcon from "@material-ui/icons/ExitToApp";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Tooltip from "@material-ui/core/Tooltip";

export const UserInfo = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const { logout, user } = useSession();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    logout();
    handleClose();
  };

  return (
    <div>
      <Tooltip title={user.webId}>
        <Button
          disableRipple
          startIcon={<Avatar variant="rounded" src={user.imageSrc} />}
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          {user.name}
        </Button>
      </Tooltip>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={handleLogout}>
          <ListItemIcon>
            <LogoutIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText primary="Log out" secondary={user.name} />
        </MenuItem>
      </Menu>
    </div>
  );
};
