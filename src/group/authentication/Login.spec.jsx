import { useSession } from "../../authentication";
import { shallow } from "enzyme";
import React from "react";
import { Login } from "./Login";
import Button from "@material-ui/core/Button";

jest.mock("../../authentication");

describe("Login", () => {
  it("render button when not logged in", () => {
    useSession.mockReturnValue({
      loggedIn: false,
    });
    const result = shallow(<Login />);
    expect(result.find(Button)).toExist();
  });

    it("button triggers a login", () => {
        const login = jest.fn();
        useSession.mockReturnValue({
            login,
        });
        const result = shallow(<Login />);
        result.simulate('click');
        expect(login).toHaveBeenCalled();
    });

  it("render nothing when logged in", () => {
    useSession.mockReturnValue({
      loggedIn: true,
    });
    const result = shallow(<Login />);
    expect(result).toBeEmptyRender();
  });
});
