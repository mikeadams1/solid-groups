import { useSession } from "../../authentication";
import { shallow } from "enzyme";
import React from "react";
import { Login } from "./Login";
import { LoginStatus } from "./LoginStatus";
import { UserInfo } from "./UserInfo";

jest.mock("../../authentication");

describe("LoginStatus", () => {
  it("render Login when not logged in", () => {
    useSession.mockReturnValue({
      loggedIn: false,
    });
    const result = shallow(<LoginStatus />);
    expect(result.find(Login)).toExist();
  });

  it("render user when logged in", () => {
    useSession.mockReturnValue({
      loggedIn: true,
      user: {
        name: "Alice",
      },
    });
    const result = shallow(<LoginStatus />);
    const userInfo = result.find(UserInfo);
    expect(userInfo).toExist();
    expect(userInfo).toHaveProp("name", "Alice");
  });
});
