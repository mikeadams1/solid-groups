import { useServiceContext } from "../../service";
import { useSession } from "../../authentication";
import { useJoinRequest } from "./useJoinRequest";
import { withHook } from "../../test-utils";
import { act } from "react-dom/test-utils";
import { useUserFeedback } from "../../error";

jest.mock("../../authentication");
jest.mock("../../service");
jest.mock("../../error");

describe("use join request", () => {
  let render, renderCycle;
  let sendJoinRequest;
  let group, session;
  beforeEach(() => {
    jest.resetAllMocks();
    sendJoinRequest = jest.fn();
    useServiceContext.mockReturnValue({ sendJoinRequest });
    // given a user
    session = { user: { name: "Jane" } };
    useSession.mockReturnValue(session);
    // and a group to join
    group = {
      uri: "https://group.example/group/main#we",
    };
    ({ render, renderCycle } = withHook(useJoinRequest, group));
  });

  describe("when a join requested successfully", () => {
    const INITIAL = 0;
    const JOIN_REQUESTED = 1;
    const REQUEST_FINISHED = 2;

    beforeEach(async () => {
      sendJoinRequest.mockResolvedValue("ok");
      render();
      await act(async () => {
        await renderCycle[INITIAL].joinRequested();
      });
    });

    it("does not indicate loading initially", () => {
      expect(useUserFeedback.mock.calls[INITIAL][0]).toEqual({
        loading: false,
      });
    });

    it("sends a join requests", () => {
      expect(sendJoinRequest).toHaveBeenCalledWith(group, session.user);
    });

    it("indicates loading while request is sent", () => {
      expect(useUserFeedback.mock.calls[JOIN_REQUESTED][0]).toEqual({
        loading: true,
      });
    });

    it("indicates success after request finished", () => {
      expect(useUserFeedback.mock.calls[REQUEST_FINISHED][0]).toEqual({
        loading: false,
        value: {
          message: "Your join request has been sent.",
        },
      });
    });
  });

  describe("when loading fails", () => {
    const INITIAL = 0;
    const JOIN_REQUESTED = 1;
    const REQUEST_FINISHED = 2;

    beforeEach(async () => {
      sendJoinRequest.mockRejectedValue(new Error("oh oh"));
      render();
      await act(async () => {
        await renderCycle[INITIAL].joinRequested();
      });
    });

    it("does not indicate loading initially", () => {
      expect(useUserFeedback.mock.calls[INITIAL][0]).toEqual({
        loading: false,
      });
    });

    it("indicates loading while request is sent", () => {
      expect(useUserFeedback.mock.calls[JOIN_REQUESTED][0]).toEqual({
        loading: true,
      });
    });

    it("indicates error after request finished", () => {
      expect(useUserFeedback.mock.calls[REQUEST_FINISHED][0]).toEqual({
        loading: false,
        error: new Error("oh oh"),
      });
    });
  });
});
