import React from "react";

import "typeface-roboto";
import CssBaseline from "@material-ui/core/CssBaseline";
import { HashRouter as Router, Route, Switch } from "react-router-dom";

import { SetupGroup } from "./setup/SetupGroup";
import { Session } from "./authentication";
import { Main } from "./Main";
import { ServiceContext } from "./service";
import { Feedback } from "./error";

function App({ service, authService }) {
  return (
    <ServiceContext.Provider value={service}>
      <CssBaseline />
      <Feedback>
        <Session authService={authService}>
          <Router>
            <Switch>
              <Route exact path="/setup">
                <SetupGroup />
              </Route>
              <Route path="/">
                <Main />
              </Route>
            </Switch>
          </Router>
        </Session>
      </Feedback>
    </ServiceContext.Provider>
  );
}

export default App;
