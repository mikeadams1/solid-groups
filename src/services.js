import * as solid from './service/solid';
import * as solidAuth from './service/solid/authentication';

import * as inMem from './service/inMem'
import * as inMemAuth from './service/inMem/authentication'

const useInMem = process.env.REACT_APP_USE_IN_MEM_SERVICE;

const service = useInMem ? inMem : solid;
const authService = useInMem ? inMemAuth : solidAuth;

export {
    service,
    authService
}
