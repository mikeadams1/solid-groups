
export const deleted = (request) => ({
    type: "deleted",
    payload: request,
});

export default (state, { type, payload }) => {
  switch (type) {
    case "deleted": {
      return [...state.filter((it) => it.uri !== payload.uri)];
    }
    default: {
      return state;
    }
  }
};
