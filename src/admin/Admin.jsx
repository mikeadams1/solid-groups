import {Layout} from "../layout/Layout";
import React from "react";
import {JoinRequests} from "./JoinRequests";

export const Admin = ({uri}) => {
    return <Layout>
        <JoinRequests group={{uri}} />
    </Layout>
}