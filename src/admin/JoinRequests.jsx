import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import { JoinRequest } from "./JoinRequest";
import ListSubheader from "@material-ui/core/ListSubheader";
import { useJoinRequests } from "./useJoinRequests";
import LinearProgress from "@material-ui/core/LinearProgress";
import { PageError } from "../error/PageError";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 600,
    backgroundColor: theme.palette.background.paper,
  },
}));

export const PageLoading = () => <LinearProgress color="secondary" />;

export const JoinRequests = ({ group }) => {
  const classes = useStyles();
  const { requests, accepted, rejected, loading, error } = useJoinRequests(
    group
  );

  if (loading) return <PageLoading />;
  if (error) return <PageError error={error} />;

  return (
    <List
      className={classes.root}
      subheader={<ListSubheader>{requests.length} Join Requests</ListSubheader>}
    >
      {requests.map((request) => (
        <JoinRequest
          key={request.uri}
          group={group}
          onRejected={rejected}
          onAccepted={accepted}
          {...request}
        />
      ))}
    </List>
  );
};
