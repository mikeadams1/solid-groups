import {useServiceContext} from "../service";
import {useJoinRequests} from "./useJoinRequests";
import {withHook} from "../test-utils";
import {act} from "react-dom/test-utils";

jest.mock("../service");

describe("useJoinRequests", () => {
  let render, renderCycle;
  let loadJoinRequests;
  beforeEach(() => {
    loadJoinRequests = jest.fn();
    useServiceContext.mockReturnValue({ loadJoinRequests });
    ({ render, renderCycle } = withHook(useJoinRequests, {
      uri: "https://group.example/group/main#we",
    }));
  });

  describe("when loading successfully", () => {
    const INITIAL = 0;
    const WHILE_LOADING = 1;
    const AFTER_LOADING = 2;

    beforeEach(async () => {
      loadJoinRequests.mockResolvedValue(["one", "two", "three"]);
      await act(async () => {
        render();
      });
    });

    it("indicates loading initially", () => {
      expect(renderCycle[INITIAL].loading).toBe(true);
    });

    it("indicates loading while loading", () => {
      expect(renderCycle[WHILE_LOADING].loading).toBe(true);
    });

    it("returns the join requests when finished", async () => {
      expect(renderCycle[AFTER_LOADING].requests).toEqual([
        "one",
        "two",
        "three",
      ]);
    });

    it("stops loading indicator when finished", async () => {
      expect(renderCycle[AFTER_LOADING].loading).toBe(false);
    });
  });

  describe("when loading fails", () => {
    const INITIAL = 0;
    const WHILE_LOADING = 1;
    const AFTER_LOADING = 2;

    beforeEach(async () => {
      loadJoinRequests.mockRejectedValue(new Error("no no no"));
      await act(async () => {
        render();
      });
    });

    it("indicates loading initially", () => {
      expect(renderCycle[INITIAL].loading).toBe(true);
    });

    it("indicates loading while loading", () => {
      expect(renderCycle[WHILE_LOADING].loading).toBe(true);
    });

    it("indicates error when finished", async () => {
      expect(renderCycle[AFTER_LOADING].error).toEqual(new Error("no no no"));
    });

    it("stops loading indicator when finished", async () => {
      expect(renderCycle[AFTER_LOADING].loading).toBe(false);
    });
  });
});
