import { act } from "react-dom/test-utils";
import { useJoinRejection } from "./useJoinRejection";
import { useServiceContext } from "../service";
import { useSession } from "../authentication";
import { withHook } from "../test-utils";
import { useUserFeedback } from "../error";

jest.mock("../authentication");
jest.mock("../service");
jest.mock("../error");

describe("use join rejection", () => {
  let render, renderCycle;
  let sendJoinRejection, loadProfile, deleteJoinRequest, onRejected, successConfirmed;
  let request, session;
  beforeEach(() => {
    jest.resetAllMocks();
    sendJoinRejection = jest.fn();
    loadProfile = jest.fn();
    deleteJoinRequest = jest.fn();
    successConfirmed = jest.fn();
    onRejected = jest.fn();
    useServiceContext.mockReturnValue({
      sendJoinRejection,
      loadProfile,
      deleteJoinRequest,
    });
    useUserFeedback.mockReturnValue({ successConfirmed });
    // given an admin user
    session = { user: { name: "Admin" } };
    useSession.mockReturnValue(session);
    // and a group
    const group = {
      uri: "https://group.example/group/main#we",
    };
    // and a person wanting to join
    const person = { name: "Jane" };
    loadProfile.mockResolvedValue(person);
    request = { uri: "https://group.example/inbox/request.ttl", group, person };
    ({ render, renderCycle } = withHook(useJoinRejection, request, onRejected));
  });

  describe("when a join rejected successfully", () => {
    const INITIAL = 0;
    const JOIN_REJECTED = 1;

    beforeEach(async () => {
      sendJoinRejection.mockResolvedValue("ok");
      render();
      await act(async () => {
        await renderCycle[INITIAL].joinRejected();
      });
    });

    it("does not indicate loading initially", () => {
      expect(useUserFeedback.mock.calls[INITIAL][0]).toEqual({
        loading: false,
      });
    });

    it("sends a join rejection", () => {
      expect(sendJoinRejection).toHaveBeenCalledWith(
        session.user,
        request.group,
        request.person
      );
    });

    it("deletes the join request", () => {
      expect(deleteJoinRequest).toHaveBeenCalledWith(request);
    });

    it("indicates loading while rejection is sent", () => {
      expect(useUserFeedback.mock.calls[JOIN_REJECTED][0]).toEqual({
        loading: true,
      });
    });

    it('calls onRejected callback', () => {
      expect(onRejected).toHaveBeenCalledWith(request);
    });

    it("indicates success after rejection finished", () => {
      expect(successConfirmed).toHaveBeenCalledWith(
        "You rejected the group join."
      );
    });
  });

  describe("when loading fails", () => {
    const INITIAL = 0;
    const JOIN_REJECTED = 1;
    const REJECTION_FINISHED = 2;

    beforeEach(async () => {
      sendJoinRejection.mockRejectedValue(new Error("oh oh"));
      render();
      await act(async () => {
        await renderCycle[INITIAL].joinRejected();
      });
    });

    it("does not indicate loading initially", () => {
      expect(useUserFeedback.mock.calls[INITIAL][0]).toEqual({
        loading: false,
      });
    });

    it("indicates loading while rejection is sent", () => {
      expect(useUserFeedback.mock.calls[JOIN_REJECTED][0]).toEqual({
        loading: true,
      });
    });

    it("indicates error after rejection finished", () => {
      expect(useUserFeedback.mock.calls[REJECTION_FINISHED][0]).toEqual({
        loading: false,
        error: new Error("oh oh"),
      });
    });
  });
});
