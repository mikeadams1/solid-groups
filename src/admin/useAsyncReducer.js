import { useCallback, useEffect, useReducer } from "react";

export default (reducer, initialState, asyncFn, deps) => {
  const asyncReducer = (state, { type, payload }) => {
    switch (type) {
      case "loading": {
        return { ...state, loading: true };
      }
      case "success": {
        return { ...state, loading: false, data: payload };
      }
      case "error": {
        return { ...state, loading: false, error: payload };
      }
      default: {
        return {
          ...state,
          data: reducer(state.data, { type, payload })
        };
      }
    }
  };
  const [state, dispatch] = useReducer(asyncReducer, {
    loading: true,
    error: false,
    data: initialState,
  });

  const callback = useCallback(asyncFn, deps);

  useEffect(() => {
    const fetch = async () => {
      dispatch({ type: "loading" });
      try {
        const result = await callback();
        dispatch({ type: "success", payload: result });
      } catch (error) {
        dispatch({ type: "error", payload: error });
      }
    };
    fetch();
  }, [callback]);

  return [state, dispatch];
};
