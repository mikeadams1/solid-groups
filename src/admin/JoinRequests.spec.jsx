import React from "react";
import { useJoinRequests } from "./useJoinRequests";
import { JoinRequests, PageLoading } from "./JoinRequests";
import { JoinRequest } from "./JoinRequest";
import { shallow } from "enzyme";
import { PageError } from "../error/PageError";

jest.mock("./useJoinRequests");

describe("JoinRequests", () => {
  it("render each join request", () => {
    useJoinRequests.mockReturnValue({
      requests: [
        {
          person: {
            webId: "https://john.example/",
            name: "John",
          },
        },
        {
          person: {
            webId: "https://jane.example/",
            name: "Jane",
          },
        },
      ],
    });
    const result = shallow(
      <JoinRequests group={{ uri: "https://group.example/" }} />
    );
    const requests = result.find(JoinRequest);
    expect(requests).toHaveLength(2);
    expect(requests.at(0)).toHaveProp("person", {
      webId: "https://john.example/",
      name: "John",
    });
    expect(requests.at(1)).toHaveProp("person", {
      webId: "https://jane.example/",
      name: "Jane",
    });
  });

  it("should render empty list", () => {
    useJoinRequests.mockReturnValue({ requests: [] });
    const result = shallow(
      <JoinRequests group={{ uri: "https://group.example/" }} />
    );
    const requests = result.find(JoinRequest);
    expect(requests).not.toExist();
  });

  it("should call hook for group", () => {
    useJoinRequests.mockReturnValue({ requests: [] });
    shallow(<JoinRequests group={{ uri: "https://group.example/" }} />);
    expect(useJoinRequests).toHaveBeenCalledWith({
      uri: "https://group.example/",
    });
  });

  it("renders loading indicator while loading", () => {
    useJoinRequests.mockReturnValue({
      loading: true,
      error: false,
    });
    const result = shallow(<JoinRequests />);
    expect(result).toContainReact(<PageLoading />);
  });

  it("renders error indicator when loading failed", () => {
    const error = new Error("oops");
    useJoinRequests.mockReturnValue({
      loading: false,
      error,
    });
    const result = shallow(<JoinRequests />);
    expect(result).toContainReact(<PageError error={error} />);
  });
});
