import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import LinkIcon from "@material-ui/icons/Link";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import PersonAddDisabledIcon from "@material-ui/icons/PersonAddDisabled";
import ListItemText from "@material-ui/core/ListItemText";
import React from "react";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import Chip from "@material-ui/core/Chip";
import IconButton from "@material-ui/core/IconButton";
import { useJoinRequest } from "./useJoinRequest";

export const JoinRequest = ({ group, uri, summary, person, onAccepted, onRejected }) => {
  const request = { group, uri, summary, person };
  const { joinRejected, joinAccepted } = useJoinRequest(request, onAccepted, onRejected);
  return (
    <ListItem key={person.webId}>
      <ListItemAvatar>
        <Avatar />
      </ListItemAvatar>
      <ListItemText
        id={person.webId}
        primary={summary}
        secondary={
          <Chip
            variant="outlined"
            icon={<LinkIcon />}
            size="small"
            clickable
            component="a"
            href={person.webId}
            label={person.webId}
          />
        }
      />
      <ListItemSecondaryAction>
        <IconButton component="a" href={uri} aria-label="Resource URI">
          <LinkIcon />
        </IconButton>
        <IconButton onClick={joinAccepted} aria-label="Accept and add to group">
          <PersonAddIcon />
        </IconButton>
        <IconButton onClick={joinRejected} aria-label="Reject to join">
          <PersonAddDisabledIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  );
};
