import { useAsyncFn } from "react-use";
import { useServiceContext } from "../service";
import { useSession } from "../authentication";
import { useUserFeedback } from "../error";

export const useJoinAcceptance = (request, onAccepted) => {
  const { sendJoinAcceptance, loadProfile, deleteJoinRequest, addToGroup} = useServiceContext();
  const { user: admin } = useSession();

  const [acceptanceState, joinAccepted] = useAsyncFn(async () => {
    const person = await loadProfile(request.person.webId);
    await sendJoinAcceptance(admin, request.group, person);
    await deleteJoinRequest(request);
    await addToGroup(request.person, request.group)
    onAccepted(request);
    successConfirmed("You accepted the group join.");
  }, [admin, request, onAccepted]);

  const { successConfirmed } = useUserFeedback(acceptanceState);

  return {
    joinAccepted,
  };
};
