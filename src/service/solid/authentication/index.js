import * as auth from "solid-auth-client";

export { default as login } from "./login";

export { default as trackSession } from "./trackSession";

export const logout = () => auth.logout();
