import fileClient from "../file-client";

export default async (request) => {
  return fileClient.deleteFile(request.uri);
};
