import fileClient from "../file-client";

const acceptToJoin = (admin, group) => (person) => `
@prefix :      <#> .
@prefix as:    <https://www.w3.org/ns/activitystreams#> .

:it
    a as:Accept ;
    as:actor <${admin.webId}> ;
    as:object :join ;
    as:summary "${admin.name} accepted your join request" .

:join
    a as:Join ;
    as:actor <${person.webId}> ;
    as:object <${group.uri}> ;
    as:summary "${person.name} joins group" .`;

export default async (admin, group, person) => {
  if (!person.inbox) {
    throw new Error ("The inbox of the person to accept is unknown.");
  }
  const url = new URL("acceptance", person.inbox).href;
  const body = acceptToJoin(admin, group);
  return fileClient.postFile(url, body(person), "text/turtle", {createPath: false});
}
