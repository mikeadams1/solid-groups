import data from "@solid/query-ldflex";

export default async (file) => {
  const subjects = await data[file].subjects.values;
  const offer = subjects[0];
  const webId = await data[offer].as_actor.value;
  const summary = await data[offer].as_summary.value;
  return {
    uri: offer,
    summary,
    person: {
      webId,
    },
  };
};
