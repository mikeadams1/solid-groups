import data from "@solid/query-ldflex";
import loadJoinRequest from "./loadJoinRequest";

export default async (groupUri) => {
  const inbox = await data[groupUri].ldp_inbox.value;
  const files = await data[inbox].ldp_contains.values;
  const promises = [];
  for (const file of files) {
    let request = loadJoinRequest(file);
    promises.push(request);
  }
  return await allSuccessful(promises);
};

async function allSuccessful(promises) {
  return (await Promise.allSettled(promises)).filter(fulfilled).map(value);
}

const fulfilled = (it) => it.status === "fulfilled";
const value = (it) => it.value;
