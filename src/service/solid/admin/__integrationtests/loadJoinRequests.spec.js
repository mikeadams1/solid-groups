/**
 * @jest-environment node
 */

import loadJoinRequests from "../loadJoinRequests";
import { givenMolid } from "molid/lib/molid-jest";

describe("load join requests", () => {
  givenMolid("with some join requests", (molid) => {
    describe("when loading join requests for a group", () => {
      let result;
      beforeAll(async () => {
        result = await loadJoinRequests(molid.uri("/groups#1"));
      });

      it("then it returns an array of requests", async () => {
        expect(result).toHaveLength(3);
      });

      it("and it contains alice's request", async () => {
        expect(result).toContainEqual({
          uri: molid.uri('/inbox/1337/join-offer-1.ttl#it'),
          summary: 'Alice asks to join the group',
          person: {
            webId: molid.uri("/profile/alice#me"),
          },
        });
      });

      it("and it contains bobs's request", async () => {
        expect(result).toContainEqual({
          uri: molid.uri('/inbox/1337/join-offer-2.ttl#it'),
          summary: 'Bob asks to join the group',
          person: {
            webId: molid.uri("/profile/bob#me"),
          },
        });
      });

      it("and it contains claire's request", async () => {
        expect(result).toContainEqual({
          uri: molid.uri('/inbox/1337/join-offer-3.ttl#it'),
          summary: 'Claire asks to join the group',
          person: {
            webId: molid.uri("/profile/claire#me"),
          },
        });
      });
    });
  });
});
