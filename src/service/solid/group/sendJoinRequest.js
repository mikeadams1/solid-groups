import fileClient from "../file-client";

const offerToJoin = (group) => (person) => `
@prefix :      <#> .
@prefix as:    <https://www.w3.org/ns/activitystreams#> .

:it
    a as:Offer ;
    as:actor <${person.webId}> ;
    as:object :join ;
    as:summary "${person.name} asks to join the group" ;
    as:target <${group.uri}> .

:join
    a as:Join ;
    as:actor <${person.webId}> ;
    as:object <${group.uri}> ;
    as:summary "${person.name} joins group" .`;

const joinOfferResource = (group) => ({
  url: new URL("join-offer", group.inbox).href,
  body: offerToJoin(group),
  contentType: "text/turtle",
});

export function sendJoinRequest(group, person) {
  const resource = joinOfferResource(group);
  return fileClient.postFile(
    resource.url,
    resource.body(person),
    resource.contentType,
    { createPath: false }
  );
}
