import data from "@solid/query-ldflex";

export async function loadGroup(uri) {
  const name = await data[uri].vcard_fn.value;
  const inbox = await data[uri].ldp_inbox.value;
  const members = await data[uri].vcard_hasMember.values;
  return {
    uri,
    name,
    inbox,
    members: members.map((uri) => ({
      webId: uri,
    })),
  };
}
