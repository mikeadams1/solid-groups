import data from "@solid/query-ldflex";

export default async (uri) => {
  try {
    const type = await data[uri].type.value;
    const inbox = await data[uri].ldp_inbox.value;

    return {
      exists: !!type,
      isGroup: type === "http://www.w3.org/2006/vcard/ns#Group",
      hasInbox: !!inbox,
    };
  } catch (e) {
    return {
      exists: false,
    };
  }
};
