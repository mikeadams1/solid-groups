import data from "@solid/query-ldflex";
import {v4 as uuidv4} from "uuid";
import fileClient from "../../file-client";
import setInboxPermissions from "./setInboxPermissions";

export const createInbox = async (groupUri) => {
    const inboxUri = await linkGroupToNewInbox(groupUri);
    await createInboxContainer(inboxUri);
};

async function linkGroupToNewInbox(uri) {
    await data[uri].ldp_inbox.set(data[`./inbox/${uuidv4()}/`]);
    const inbox = await data[uri].ldp_inbox;
    return inbox.value;
}

async function createInboxContainer(inboxUrl) {
    await fileClient.createFolder(inboxUrl);
    await setInboxPermissions(inboxUrl);
}
