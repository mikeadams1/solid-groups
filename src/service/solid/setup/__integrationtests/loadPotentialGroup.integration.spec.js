/**
 * @jest-environment node
 */

import loadPotentialGroup from "../loadPotentialGroup";

import { givenMolid } from "molid/lib/molid-jest";

describe("load potential group", () => {
  givenMolid("with some groups", (molid) => {
    describe("when loading a fully set up group", () => {
      let result;
      beforeAll(async () => {
        result = await loadPotentialGroup(molid.uri("/groups/full#we"));
      });

      it("exists", () => {
        expect(result.exists).toBe(true);
      });

      it("is a group", () => {
        expect(result.isGroup).toBe(true);
      });

      it("has an inbox", () => {
        expect(result.hasInbox).toBe(true);
      });
    });

    describe("when loading a minimal group", () => {
      let result;
      beforeAll(async () => {
        result = await loadPotentialGroup(molid.uri("/groups/minimal#we"));
      });

      it("exists", () => {
        expect(result.exists).toBe(true);
      });

      it("is a group", () => {
        expect(result.isGroup).toBe(true);
      });

      it("has no inbox", () => {
        expect(result.hasInbox).toBe(false);
      });
    });

    describe("when loading a non-group resource", () => {
      let result;
      beforeAll(async () => {
        result = await loadPotentialGroup(molid.uri("/groups/person#me"));
      });

      it("exists", () => {
        expect(result.exists).toBe(true);
      });

      it("is a group", () => {
        expect(result.isGroup).toBe(false);
      });
    });

    describe("when loading a non-existing group", () => {
      let result;
      beforeAll(async () => {
        result = await loadPotentialGroup(molid.uri("/groups/new#we"));
      });

      it("does not exist", () => {
        expect(result.exists).toBe(false);
      });
    });
  });
});
