import * as auth from "solid-auth-client";
import FC from "solid-file-client";

export default new FC(auth);