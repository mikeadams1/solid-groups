export const persons = {
  "https://alice.example": {
    webId: "https://alice.example",
    name: "Alice",
  },
  "https://bob.example": {
    webId: "https://bob.example",
    imageSrc: "https://picsum.photos/300?blur=10",
    name: "Bob",
  },
  "https://claire.example": {
    webId: "https://claire.example",
    imageSrc: "https://picsum.photos/200?blur=10",
    name: "Claire",
  },
};

export default async (webId) => Promise.resolve(persons[webId]);
