export default async (uri) => {
    return {
      exists: true,
      isGroup: true,
      hasInbox: true
    }
};
