let handleLogin = () => null;
let handleLogout = () => null;

export const trackSession = (onLogin, onLogout) => {
    handleLogin = onLogin
    handleLogout = onLogout
}

export const login = () => {
    handleLogin({
        webId: 'https://jane.example',
        name: 'Jane Doe'
    })
    return Promise.resolve()
};

export const logout = () => {
    handleLogout();
    return Promise.resolve();
}
