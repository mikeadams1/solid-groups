import {persons} from "../person/loadProfile";

export const requests = [
  {
    uri: "https://group.example/inbox/0",
    summary: "Alice wants to join the group.",
    person: persons["https://alice.example"],
  },
  {
    uri: "https://group.example/inbox/1",
    summary: "Bob wants to join the group.",
    person: persons["https://bob.example"],
  },
];

export async function sendJoinRequest(group, person) {
  persons[person.webId] = person;
  requests.push({
    uri: `https://group.example/inbox/${requests.length}`,
    summary: `${person.name} wants to join ${group.name}`,
    person: person
  })
}
