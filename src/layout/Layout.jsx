import Container from "@material-ui/core/Container";
import {Header} from "../group/header/Header";
import {Footer} from "./Footer";
import React from "react";
import {LoginStatus} from "../group/authentication/LoginStatus";

export const Layout = ({ children }) => {
    return (
        <>
            <Container maxWidth="lg">
                <Header>
                    <LoginStatus/>
                </Header>
                <main>
                    {children}
                </main>
            </Container>
            <Footer title="Solid Groups" description="The Web is social." />
        </>
    );
};