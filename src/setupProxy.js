let uri = process.env.REACT_APP_GROUP_URI || "";

module.exports = function (app) {
  app.use(
      "/config.js",
      (req, res) => {
          if (req.method === 'PUT') {
              uri = ""
          }
          res.header('Content-Type', 'application/javascript');
          res.send(`window.SOLID_GROUPS_MAIN_URI="${uri}"`)
      }
  );
};
