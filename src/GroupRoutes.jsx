import { Group } from "./group/Group";
import { Admin } from "./admin/Admin";
import React from "react";
import { Route } from "react-router-dom";

export const GroupRoutes = ({ uri }) => (
  <>
    <Route exact path="/">
      <Group uri={uri} />
    </Route>
    <Route exact path="/admin">
      <Admin uri={uri} />
    </Route>
  </>
);
