import * as auth from "solid-auth-client";

export const putConfig = (uri) =>
  auth
    .fetch("config.js", {
      method: "PUT",
      headers: {
        "Content-Type": "application/javascript",
      },
      body: `window.SOLID_GROUPS_MAIN_URI="${uri}"`,
    })
    .then((response) => {
      if (!response.ok) {
        throw new Error(response.statusText);
      }
      return response;
    });
