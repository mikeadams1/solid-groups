import React from "react";
import Container from "@material-ui/core/Container";
import Avatar from "@material-ui/core/Avatar";
import GroupIcon from "@material-ui/icons/Group";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { SetupGroupForm } from "./SetupGroupForm";
import { Login } from "./Login";
import { useSession } from "../authentication";
import {Redirect} from "react-router-dom";
import config from "../config";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
}));

export const SetupGroup = () => {
  const classes = useStyles();
  const { loggedIn } = useSession();
  if (config.uri) {
    return <Redirect to="/" />
  }
  return (
    <Container maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <GroupIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Set up your first group
        </Typography>
        {loggedIn ? <SetupGroupForm className={classes.form} /> : <Login />}
      </div>
    </Container>
  );
};
