import React from "react";
import { shallow } from "enzyme";
import { Main } from "./Main";
import { Redirect } from "react-router-dom";
import config from "./config";
import { GroupRoutes } from "./GroupRoutes";

jest.mock("./config", () => ({}));

describe("Home", () => {
  it("asks to set up group if none is set up yet", () => {
    const result = shallow(<Main />);
    expect(result).toContainReact(<Redirect to="/setup" />);
  });

  it("shows group if one has been set up", () => {
    config.uri = "https://group.example/#we";
    const result = shallow(<Main />);
    expect(result).toContainReact(
      <GroupRoutes uri="https://group.example/#we" />
    );
  });
});
