import { useEffect, useReducer } from "react";
import reducer, { ANONYMOUS, login, logout } from "./reducer";

export const useAuthentication = (authService) => {
  const [session, dispatch] = useReducer(reducer, {
    user: ANONYMOUS,
    loggedIn: false,
  });

  useEffect(() => {
    authService.trackSession(
      (user) => dispatch(login(user)),
      () => dispatch(logout())
    );
  }, [authService]);

  return {
    ...session,
    login: () => {
      authService.login();
    },
    logout: () => {
      authService.logout();
    },
  };
};
