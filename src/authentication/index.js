import * as React from "react";
import {useAuthentication} from "./useAuthentication";
import {useContext} from "react";

const SessionContext = React.createContext({
    webId: null
});

export const Session = ({ children, authService }) => {
    const session = useAuthentication(authService);
    return (
        <SessionContext.Provider value={session}>
            {children}
        </SessionContext.Provider>
    );
};

export const useSession = () => useContext(SessionContext);