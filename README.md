# Solid Groups

Form groups all around the Web to bring people together. 👫👫

- Create and host a group on any Solid Pod 🌐
- Allow people to join your group using their WebID

## What is the purpose?

Solid is an open ecosystem with people and data all around the Web.
Therefore, it can be hard to find people (and their WebIDs) to connect with them in some way.

Solid Groups bring people together, that have something in common. Those people can then interact and communicate in
any way the Solid ecosystem provides, e.g.

- Start a chat 💬
- Organize events 🥂
- Share photo galleries 📸
- Collaborate on documents 📝
- ...

Those things are not part of the Solid Groups app, but can be achieved with other Solid apps that exist or will exist in the future.

## Demo group

You can try the app and join the demo group at https://solid-groups.solidcommunity.net/

## Deploy to your Pod

tl;dr:

1.  `git clone https://gitlab.com/angelo-v/solid-groups.git`
2.  Adjust `.env` file
3.  `npm run deploy`

### 1. Checkout this repo

```bash
git clone https://gitlab.com/angelo-v/solid-groups.git
```

### 2. Adjust then `.env` file:

Open the file `.env` in the root of this repo and adjust the variables:

`DEPLOY_URL` is the URL you want to deploy to. This could be the root of a group pod, but also any subdirectory within your Pod.

`SOLID_IDP` has to contain the URL of your Identity Provider
(e.g. https://solidcommunity.net).

`SOLID_USERNAME` is the username you are using to log in to your Pod.

The password does not need to be configured. It will be prompted.

### 3. Publish

Run `npm run deploy` to start the build process and upload your homepage
to the Pod. You will be prompted for your password.

## Development notes

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
